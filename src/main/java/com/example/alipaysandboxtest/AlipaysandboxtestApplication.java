package com.example.alipaysandboxtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlipaysandboxtestApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlipaysandboxtestApplication.class, args);
    }

}
