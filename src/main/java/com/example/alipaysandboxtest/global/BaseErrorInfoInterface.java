package com.example.alipaysandboxtest.global;

/**
 * @description: 服务接口类
 * @author: YangLi
 * @date: 2023/7/20
 */
public interface BaseErrorInfoInterface {

    /**
     * 错误码
     *
     * @return ·
     */
    String getResultCode();

    /**
     * 错误描述
     *
     * @return ·
     */
    String getResultMsg();
}
