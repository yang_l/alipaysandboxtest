package com.example.alipaysandboxtest.global;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class MyException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private String msg;
    private Integer code;

    public MyException(String msg,Throwable cause) {
        super(msg,cause);
        this.msg = msg;
    }

    public MyException(String msg) {
        this.code = 500;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
