package com.example.alipaysandboxtest.response;

import lombok.Data;

/**
 * @Author YangLi
 * @Date 2024/1/30 14:26
 * @注释
 */
@Data
public class Result<T> {

    private final Integer code;

    private final String message;

    private T data;

    public Result(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Result(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public static <t> Result<t> ok(t data){
        return new Result<>(200, "操作成功", data);
    }

    public static <t> Result<t> ok(){
        return new Result<>(200, "操作成功");
    }


    public static <t> Result<t> error(){
        return new Result<>(500, "操作失败");
    }


}
